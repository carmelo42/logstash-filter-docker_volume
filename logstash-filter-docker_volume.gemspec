Gem::Specification.new do |s|
  s.name = 'logstash-filter-docker_volume'
  s.version = '0.1.6'
  s.licenses = ['Apache License (2.0)']
  s.summary = "Gets Docker container_mount_point's path using the container_id field."
  s.description = "This gem is a logstash plugin for Docker. It reads the container_id field, and creates the container_volume one. Its content is the path of the container_mount_point persistant volume."
  s.authors = ["Mikael Davranche"]
  s.email = 'mikael.davranche@ovhcloud.com'
  s.homepage = "https://gitlab.com/mdavranche/logstash-filter-docker_volume"
  s.require_paths = ["lib"]

  # Files
  s.files = Dir["CONTRIBUTORS","Gemfile","lib/**/*","LICENSE","*.gemspec","Rakefile","*.md"]

  # Tests
  s.test_files = s.files.grep(%r{^(test|spec|features)/})

  # Special flag to let us know this is actually a logstash plugin
  s.metadata = { "logstash_plugin" => "true", "logstash_group" => "filter" }

  # Gem dependencies
  s.add_runtime_dependency "logstash-core", '>= 5.0.0', '< 7.0.0'
  s.add_development_dependency 'logstash-devutils'
end
