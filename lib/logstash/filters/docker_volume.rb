# encoding: utf-8
require 'logstash/filters/base'
require 'logstash/namespace'
require 'logstash/filters/docker_volume_inspector'
require 'json'

class LogStash::Filters::DockerVolume < LogStash::Filters::Base

  config_name "docker_volume"

  config :match, :validate => :hash, :default => {'container_id' => 'container_volume'}, :required => true

  config :docker_client, :validate => :string, :default => 'docker'

  config :client_options, :validate => :string, :default => ''

  # The volume mount point is required to get the corresponding volume path
  config :volume_mount_point, :validate => :string, :required => true

  public
  def register
    # Add instance variables
    @inspector = LogStash::Filters::DockerVolumeSupport::DockerVolumeInspector.new(@docker_client, @client_options)
    @cached = Hash.new
  end # def register

  public
  def filter(event)
    return unless filter? event

    @match.each { |in_field,out_field|
      event.set(out_field, resolve_from(event.get(in_field)))
    }

    filter_matched(event)
  end

  def resolve_from(container_id)
    if @cached.has_key?(container_id)
      return @cached[container_id]
    end

    blob = @inspector.inspect container_id
    details = JSON.parse(blob)

    return nil if details.empty?

    # docker inspect container_id shows a "Mounts" section:
    # "Mounts": [ { "Name": "xxxid1xxx", "Source": "volume_path1", "Destination": "volume_mount_point1", "Driver": "local", "Mode": "", "RW": true },
    #             { "Name": "xxxid2xxx", "Source": "volume_path2", "Destination": "volume_mount_point2", "Driver": "local", "Mode": "", "RW": true } ]
    detail = details.first['Mounts']
    if not detail.nil?
    then
      detail.each do |x|
        return nil if x.nil?
        return nil if x["Destination"].nil?
        if x["Destination"] == @volume_mount_point
        then
          return nil if x["Source"].nil?
          @cached[container_id] = x["Source"]
          return @cached[container_id]
        end
      end
    else

      # On old Docker versions, docker inspect container_id shows a "Volumes" section:
      # "Volumes": { "volume_mount_point1": "volume_path1", "volume_mount_point2": "volume_path2", ... }
      detail = details.first['Volumes']
      return nil if detail.nil?
      return nil if detail[@volume_mount_point].nil?
      @cached[container_id] = detail[@volume_mount_point]
      return @cached[container_id]

    end
    return nil

  end # def filter
end
