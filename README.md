# Logstash Plugin

This is a plugin for [Logstash](https://github.com/elasticsearch/logstash) 5.x. If you're looking for the Logstash 1.x or 2.x version, checkout the [v0.1.3 version](https://gitlab.com/mdavranche/logstash-filter-docker_volume/tags/v0.1.3).

It is fully free and fully open source. The license is Apache 2.0, meaning you are pretty much free to use it however you want in whatever way.

# Use

This Logstash filter plugin uses Docker logs as input. Those logs should contain a "container_id" field. Using this plugin will add a new field, named "container_volume". Its content is the path of the container_mount_point persistent volume (container_mount_point is the parameter you specified in logstash configuration).

# Running the plugin in Logstash

- Build your plugin gem
```
$ cd logstash-filter-docker_volume
$ gem build logstash-filter-docker_volume.gemspec
```

- Install the plugin from the Logstash home
```
$ /usr/share/logstash/bin/logstash-plugin install logstash-filter-docker_volume-0.1.5.gem
```

# Hello world

To run this 'Hello world', you must have Docker with, at least, 1 running container.

Let's grab the ID of your first container:
```
$ docker ps -q | head -n 1
d097ca8830f9
```

Note that you can use the long ID too:
```
$ docker ps --no-trunc -q | head -n 1
d097ca8830f9199e7ce7c1c3cf19f51989a3a2ce8cc2f8ac230b749c65db209b
```

The container should have, at least, 1 data volume:
```
$ docker inspect d097ca8830f9
[
{
    "Id": "d097ca8830f9199e7ce7c1c3cf19f51989a3a2ce8cc2f8ac230b749c65db209b",
    [...]
    "Mounts": [
        {
            "Name": "af35dd706b1049f0d8a833e227307b6a9d58f0c2eb8f1f29972601e87ac3e2f8",
            "Source": "/var/lib/docker/volumes/af35dd706b1049f0d8a833e227307b6a9d58f0c2eb8f1f29972601e87ac3e2f8/_data",
            "Destination": "/var/lib/mysql",
            "Driver": "local",
            "Mode": "",
            "RW": true,
            "Propagation": ""
        }
    ],
    [...]
}
]
```

For more informations on data volumes, see https://docs.docker.com/userguide/dockervolumes/.

Now, let's have a look at the Logstash's configuration. It reads on stdin and prints output on stdout. We hardcode the container ID, for tests purposes only of course.
```
input
{
  stdin { }
}
filter
{
  mutate
  {
    add_field => { "container_id" => "d097ca8830f9" }
  }
  docker_volume
  {
    volume_mount_point => "/var/lib/mysql"
  }
}
output
{
  stdout
  {
    codec => "rubydebug"
  }
}
```

Launch Logstash:
```
$ /usr/share/logstash/bin/logstash -f
```

Then, enter "Hello world!" on stdin:
```
Logstash startup completed
Hello world!
{
             "message" => "Hello world!",
            "@version" => "1",
          "@timestamp" => "2015-07-16T11:29:26.923Z",
                "host" => "myhostname.ha.ovh.net",
        "container_id" => "d097ca8830f9",
    "container_volume" => "/var/lib/docker/volumes/af35dd706b1049f0d8a833e227307b6a9d58f0c2eb8f1f29972601e87ac3e2f8/_data"
}
```

Here we are! The "container_volume" contains the path of the "/var/lib/mysql" mount point of the "d097ca8830f9" container.
